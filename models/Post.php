<?php

namespace app\models;

use Yii;
use vova07\fileapi\behaviors\UploadBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "Post".
 *
 * @property integer $id
 * @property string $title
 * @property string $excerpt
 * @property string $content
 * @property string $thumb_url
 * @property string $create_time
 * @property integer $enebled
 */
class Post extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'Post';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['excerpt', 'content'], 'string'],
            [['create_time'], 'safe'],
            [['enebled'], 'integer'],
            [['title', 'thumb_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'excerpt' => 'Краткое описание',
            'content' => 'Контент',
            'thumb_url' => 'Миниатюра',
            'create_time' => 'Create Time',
            'enebled' => 'Опубликован',
        ];
    }

    public function getStatuses($current = false) {
        $statuses = [0 => 'Черновик', 'Опубликовано'];

        if ($current)
            return $statuses[$this->status];
        else
            return $statuses;
    }

    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'thumb_url' => [
                        'path' => '@web/statics/img/upload',
                        'tempPath' => '@web/statics/temp/img/upload',
                        'url' => '/web/statics/img/upload'
                    ]
                ]
            ]
        ];
    }

}
