-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 16 2016 г., 12:38
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `kazinfo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1471181016),
('m160814_132607_create_post_table', 1471182636);

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `excerpt` text,
  `content` text,
  `thumb_url` varchar(255) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enebled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `title`, `excerpt`, `content`, `thumb_url`, `create_time`, `enebled`) VALUES
(3, 'Запись 1', '<p>Краткое1 Краткое1 Краткое1 Краткое1 Краткое1 Краткое1</p>', '<p>Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 Контент1 </p>', 'thumb1.jpg', '2016-08-15 07:28:08', 1),
(4, 'Запись 2', '<p>Краткое2 Краткое2 Краткое2 Краткое2 Краткое2 Краткое2</p>', '<p>Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 Контент2 </p>', 'thumb2.jpg', '2016-08-15 07:28:08', 1),
(5, 'Запись 3', '<p>Краткое3 Краткое3 Краткое3 Краткое3 Краткое3 Краткое3</p>', '<p>Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 Контент3 </p>', 'thumb3.jpg', '2016-08-15 07:28:08', 1),
(6, 'Запись 4', '<p>Краткое4 Краткое4 Краткое4 Краткое4 Краткое4 Краткое4 Краткое4 Краткое4 </p>', '<p>Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 Контент4 </p>', 'thumb4.jpg', '2016-08-15 07:28:08', 1),
(7, 'Запись 5', '<p>Краткое5 Краткое5 Краткое5 Краткое5 Краткое5 Краткое5 Краткое5 Краткое5 Краткое5 </p>', '<p>Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 Контент5 </p>', 'thumb5.jpg', '2016-08-15 07:28:08', 1),
(8, 'Запись 6', '<p>Краткое6 Краткое6 Краткое6 Краткое6 Краткое6 Краткое6 Краткое6 Краткое6 Краткое6 Краткое6 </p>', '<p>Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 Контент6 </p>', 'thumb6.jpg', '2016-08-15 07:28:08', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
