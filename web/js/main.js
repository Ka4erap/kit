$(document).ready(function () {
    /*Обработка события клика на копку "Подробнее*/
    $('.getContent').click(function () {
        var id = $(this).data('id');
        ajaxFunctions.getPost(id, function (response) {
            view.createModalContent(data = response);
        });
    });
    /*Обработка события клика на копку "Закрыть" моадльного окна*/
    $('#myModal close').click(function () {
        view.cleareModalContent();
    });

    $('#contact-form').on('beforeSubmit', function (e) {
        e.preventDefault();
        var url = 'site/mail';
        var form = $(this).serialize();
        ajaxFunctions.sendMail(form, url, function (response) {
            if (respone == true) {
                $('#contact-form').hide();
                var massage = 'Ваша сообщение отправлено';
                view.showMessage(message);
            } else {
                var massage = 'Произошла ошибка, попробуйте ещё раз';
                view.showMessage(message);
            }
        });
        return false;
    });
});

var view = {
    createModalContent: function (data) {
        $('#myModal #myModalLabel').text(data.title);
        var html = '<img class="thumbnail post-big-thumb" src="/statics/img/upload/' + data.thumb_url + '" />';
        html += '<div class="post-content">' + data.content + '</div>';
        $('#myModal .myModal-content').html(html);
    },
    cleareModalContent: function () {
        $('#myModal #myModalLabel').empty();
        $('#myModal .myModal-content').empty();
    },
    showMessage: function (message) {
        alert(mеssage);
    }

};

/*Ajax Функции*/
var ajaxFunctions = {
    apiUrl: 'http://kit.te/posts/',
    getPost: function (id, callback) {
        $.ajax({
            type: 'GET',
            url: this.apiUrl + id,
            dataType: 'json',
            beforeSend: function () {
                /* Пока идет запрос (показать прелоадер)*/
            },
            success: function (response) {
                callback(response);
            },
            error: function (response) {
                callback(response);
            }
        });
    },
    sendMail: function (form, url, callback) {
        $.ajax({
            method: 'POST',
            url: url,
            data: form,
            dataType: 'json',
            beforeSend: function () {
                /* Пока идет запрос (показать прелоадер)*/
            },
            success: function (response) {
                callback(response);
            },
            error: function (response) {
                callback(response);
            }
        });
    }
};

