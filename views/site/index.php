<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="row">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab1">Таб 1</a></li>
            <li><a data-toggle="tab" href="#tab2">Таб 2</a></li>
            <li><a data-toggle="tab" href="#tab3">Таб 3</a></li>
        </ul>

        <div class="tab-content">
            <div id="tab1" class="tab-pane fade in active">
                <h3>Таб 1</h3>
                <p>Контент таб 1</p>
            </div>
            <div id="tab2" class="tab-pane fade">
                <h3>Таб 2</h3>
                <p>Контент таб 2</p>
            </div>
            <div id="tab3" class="tab-pane fade">
                <h3>Таб 3</h3>
                <p>Контент таб 3</p>
            </div>
        </div> 
    </div>
    <?php foreach ($posts as $post) { ?>
        <div class="col-md-4">
            <div class="panel-body">
                <img class="thumbnail post-thumb" src="/statics/img/upload/<?= $post->thumb_url ?>" />
                <h3><?= $post->title ?></h3>
                <div class="post-excerpt"><?= $post->excerpt ?></div>
                <button type="button" class="btn btn-danger getContent" data-toggle="modal" data-target="#myModal" data-id="<?= $post->id ?>">Подробене</button>
            </div>
        </div>
    <?php } ?>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
          <div class="myModal-content"></div>
      </div
    </div>
  </div>
</div>
