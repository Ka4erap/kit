<?php

use yii\db\Migration;

/**
 * Handles the creation for table `post`.
 */
class m160814_132607_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'excerpt' => $this->text(),
            'content' => $this->text(),
            'thumb_url' =>$this->string(255),
            'create_time' => $this->timestamp(),
            'enebled' => $this->boolean()->defaultValue(FALSE)
            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
